import { Application, Router } from 'https://deno.land/x/oak/mod.ts';
import chalkin from "https://deno.land/x/chalkin/mod.ts";

// Make app
const app = new Application();

// Books
const books = new Map();
books.set('1', {
    id: '1',
    title: 'The Hound of the Baskervilles',
    author: 'Conan Doyle, Arthur'
});

// Router
const router = new Router();
router
    .get('/', ctx => {
        ctx.response.body = '<h1>Hello, World!</h1>';
    })
    .get('/book', ctx => {
        ctx.response.body = Array.from(books.values());
    })
    .get('/book/:id', ctx => {
        if (ctx.params && ctx.params.id && books.has(ctx.params.id)) {
            ctx.response.body = books.get(ctx.params.id);
        }
    });

// Add router to server
app.use(router.routes());
app.use(router.allowedMethods());

// Run
console.log();
console.log(`${chalkin.greenBright(' Server successfully started and listen on port')} ${chalkin.bold.cyanBright('7910')}`);
await app.listen({
    port: 7910
});
